#!/bin/bash
set -e

TEX_FILE=git.tex
PDF_FILE=git.pdf
LAST_COMMIT_SHA1=$(git --no-pager log -n1 --abbrev-commit --abbrev=8 --pretty=oneline ${TEX_FILE} | cut -d " " -f1)
echo "LAST_COMMIT_SHA1=${LAST_COMMIT_SHA1}"
TMP_TEX_FILE=${LAST_COMMIT_SHA1}_${TEX_FILE}

function build() {
    cp -v ${TEX_FILE} ${TMP_TEX_FILE}
    sed -i "s/ver: \(.*\)%/ver: ${LAST_COMMIT_SHA1} %/g" ${TMP_TEX_FILE}
    #build *.pdf twice
    pdflatex -shell-escape ${TMP_TEX_FILE} && pdflatex -shell-escape ${TMP_TEX_FILE}
    if [ $? -ne 0 ]; then
        echo "error! something went wrong here"
        exit 1
    else
        mv -v ${LAST_COMMIT_SHA1}_${PDF_FILE} ${PDF_FILE}
        echo "build done"
    fi
}

function clean() {
    rm -v ${LAST_COMMIT_SHA1}*
    echo "clean done"
}

build
#comment "clean" line if you want to see all build artifacts
clean

