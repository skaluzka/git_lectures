#!/bin/bash
set -e

WORK_DIR=$(pwd)
SVN_REPO=svn_repo
SVN_WORKSPACE=svn_workspace

echo "creating local svn repository..."
svnadmin create ${SVN_REPO}

echo "local checkout"
svn co file://${WORK_DIR}/${SVN_REPO}/ ${SVN_WORKSPACE}

echo "committing initial repository structure..."
cd ${SVN_WORKSPACE}
svn mkdir branches trunk tags
svn ci -m "initial project structure added"

echo "listing last changes..."
svn up && svn log -vl1 .

cd -
echo "done"

