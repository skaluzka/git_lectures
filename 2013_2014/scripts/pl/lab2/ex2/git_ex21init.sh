#!/bin/bash
set -e

GIT_REPO=git_repo

mkdir $GIT_REPO
cd $GIT_REPO
git init .
git config --local user.name $USER
git config --local user.email example@com
git status
cd -

