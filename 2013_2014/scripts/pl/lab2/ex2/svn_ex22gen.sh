#!/bin/bash
set -e

SVN_WORKSPACE=svn_workspace

cd ${SVN_WORKSPACE}/trunk
echo "generating 4k random binary files (1024B), please wait..."
time ( \
for i in {1..4000}
do
    dd if=/dev/urandom of=file_${i}.bin bs=1024 count=1 2>/dev/null
done
)
echo -e "done\ntype \"svn st $SVN_WORKSPACE\" if you want to see the results"

