#!/bin/bash
set -e

GIT_REPO=git_repo

cd $GIT_REPO
echo "generating 4k random binary files (1024B), please wait..."
time ( \
for i in {1..4000}
do
    dd if=/dev/urandom of=file_${i}.bin bs=1024 count=1 2>/dev/null
done
)
echo -e "done\ntype \"cd $GIT_REPO && git status\" if you want to see the results"

