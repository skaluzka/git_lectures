#!/bin/bash
set -e

SVN_WORKSPACE=svn_workspace

cd ${SVN_WORKSPACE}/trunk
echo "generating 4k empty files, please wait..."
time ( \
for i in {1..4000}
do
    >file_${i}.txt
done
)
echo -e "done\ntype \"svn st $SVN_WORKSPACE\" if you want to see the results"

