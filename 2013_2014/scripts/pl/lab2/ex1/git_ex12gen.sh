#!/bin/bash
set -e

GIT_REPO=git_repo

cd $GIT_REPO
echo "generating 4k empty files, please wait..."
time ( \
for i in {1..4000}
do
    >file_${i}.txt
done
)
echo -e "done\ntype \"cd $GIT_REPO && git status\" if you want to see the results"

